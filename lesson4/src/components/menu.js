import React, { Component } from 'react';
import { Link } from 'react-router-dom';

const Menu = () => (
    <ul>
        <li><Link to="/"> Go to home </Link></li>
        <li><Link to="/posts"> Go to posts </Link></li>
    </ul>
);

export default Menu;