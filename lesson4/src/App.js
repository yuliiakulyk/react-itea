import React, { Component } from 'react';
import { BrowserRouter, Route, Redirect, Switch, Link, NavLink } from 'react-router-dom';

import logo from './logo.svg';
import './App.css';
import Home from './pages/home';
import Menu from './components/menu';

class App extends Component {
  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <header>
            <Menu />
          </header>
          <Switch>
            <Route path="/" component={Home} exact/>
          </Switch>
        </BrowserRouter>
        {/* <Home /> */} 
      </div>
    );
  }
}

export default App;
