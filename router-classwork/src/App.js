import React, { Component } from 'react';
import {BrowserRouter, Route, Switch, Link, HashRouter, Redirect} from 'react-router-dom';
import logo from './logo.svg';
import './App.css';
import homePage from './pages/home';
import about from './pages/about';
import list from './pages/list';

class App extends Component {
  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <Switch>
            <Route exact path='/' component={homePage}/>
            <Route exact path='/about' component={about}/>
            <Route path='/list' component={list}/>
            <Route render={() => {return <h1>404 Not found</h1>}} />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
