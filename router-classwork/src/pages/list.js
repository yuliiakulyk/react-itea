import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, Link, HashRouter, Redirect } from 'react-router-dom';
import ArtistList from './artistList';
import ArtistAlbums from './artistAlbums';

class List extends Component {

    render() {
            return (
                <div>
                    <Switch>
                        <Route exact path="/list" component={ArtistList} />
                        <Route exact path="/list/:artistid" component={ArtistAlbums} />
                    </Switch>
                </div>
            );
        // }
    }
}

export default List;