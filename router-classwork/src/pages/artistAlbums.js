import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, Link, HashRouter, Redirect } from 'react-router-dom';

class ArtistAlbums extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        const artistid = this.props.match.params.artistid;
        let artistAlbums;
        let artistTracks;
        let artistAlbumIds;

        let promise = fetch('https://jsonplaceholder.typicode.com/albums')
            .then(res => res.json())
            .then((res) => {
                artistAlbums = res.filter(album => album.userId == artistid);
            })
            .then(() => {
                artistAlbumIds = [];
                artistAlbums.forEach(album => {
                    artistAlbumIds.push(album.id);
                });
            })
            .then(() => {
                fetch('https://jsonplaceholder.typicode.com/photos')
                    .then(res => res.json())
                    .then((res) => {
                        artistTracks = res.filter(track => artistAlbumIds.indexOf(track.albumId) > -1);
                        this.setState({ tracks: artistTracks, albums: artistAlbums });
                    });
            });
    }

    render() {
        const { albums, tracks } = this.state;

        return (albums && tracks) ? (
            <>
                <h1>Artist has {albums.length} albums:</h1>
                <ol style={{textAlign: "left", width: "70%", margin: "auto"}}>
                    {albums.map((album, index) =>
                        <li key={index}>
                            {album.title}: {tracks.filter(track => track.albumId == album.id).length} tracks
                    </li>)}
                </ol>
            </>
        ) : (
                <h1>Artist's albums and tracks loading...</h1>
            );
    }
}

export default ArtistAlbums;