import React, { Component } from 'react';
import {Link } from 'react-router-dom';

class ArtistList extends Component {
    state = {};

    componentDidMount() {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(res => res.json())
            .then(res => this.setState({ artists: res }));
    }

    render() {
        const { artists } = this.state;

        if (!artists) {
            return <h1>Artists list loading...</h1>
        } else {
            return (
                <div>
                    {artists.map((artist, index) => <p><Link to={`/list/${artist.id}`} key={index}>{artist.name}</Link></p>)}
                </div>
            )
        }
    }
}

export default ArtistList;