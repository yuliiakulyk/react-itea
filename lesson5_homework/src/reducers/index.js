
const initialState = {
    searchLoaded: true,
    searchResult: {},
    searchHistory: []
};

function reducer(state = initialState, action) {
    switch (action.type) {
        case 'SEARCH_REQ':
            return {
                ...state,
                searchLoaded: false
            };

        case 'SEARCH_RES':
            let newSearchHistory = [...state.searchHistory];
            newSearchHistory.push(action.payload);
            return {
                ...state,
                searchLoaded: true,
                searchResult: action.payload,
                searchHistory: newSearchHistory
            };

        default:
            return state;
    }
}

export default reducer;