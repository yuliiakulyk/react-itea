import React from 'react';
import {connect} from 'react-redux';

const SearchInput = ({setLoading, saveData}) => {

    const API_KEY = "ec361c6a4de88d978e3a3eff813290f5";

    const handleSearch = () => {
        //console.log('window', window);
        let query = document.getElementById('search').value;
        document.getElementById('search').value = '';
        setLoading();
        fetch(`https://api.openweathermap.org/data/2.5/weather?q=${query}&appid=${API_KEY}`)
        .then(res => res.json())
        .then(res => saveData(res));
    }

    return (
        <div>
            <input id='search' type='text' placeholder='Enter city' autoComplete='off' />
            <button onClick={handleSearch}>Search</button>
        </div>
    )
}

const mapDispatchToProps = (dispatch, ownProps) => ({
    setLoading: () => {
        dispatch({
            type: 'SEARCH_REQ'
        })
    },
    saveData: (data) => {
        dispatch({
            type: 'SEARCH_RES',
            payload: data
        })
    }
});

export default connect(null, mapDispatchToProps)(SearchInput);