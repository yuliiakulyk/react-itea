import React from 'react';
import {connect} from 'react-redux';

const SearchHistory = ({history}) => {
    return (
        <div>
            <h1>Search history</h1>
            {
                (history && history.length > 0 )
                ? (
                    history.map((record, index) => (
                        <p key={index}>{record.name}</p>
                    ))
                ) : (
                    <p>No history data yet</p>
                )
            }
        </div>
    )
}

const mapStateToProps = (state, ownProps) => ({
    history: state.searchHistory
});

export default connect(mapStateToProps, null)(SearchHistory);