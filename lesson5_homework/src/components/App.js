import React, { Component } from 'react';
import {Provider} from 'react-redux';
import store from '../redux/store';
import '../App.css';
import SearchInput from './searchInput.js';
import SearchResult from './searchResult.js';
import SearchHistory from './searchHistory.js';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Provider store={store}>
          <h1>Weather</h1>
          <SearchInput />
          <SearchResult />
          <SearchHistory />
        </Provider>
      </div>
    );
  }
}

export default App;
