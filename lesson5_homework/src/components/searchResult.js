import React from 'react';
import {connect} from 'react-redux';

const SearchResult = ({ searchResult, isLoaded }) => {
    
    if (!isLoaded) {
        return (<h2>Loading...</h2>);
    } else if (searchResult && Object.keys(searchResult).length > 0) {
        return (
            <div>
                <h2>{searchResult.name}</h2>
                <p>Temperature: {searchResult.main.temp}</p>
                <p>Pressure: {searchResult.main.pressure}</p>
                <p>Humidity: {searchResult.main.humidity}</p>
            </div>
        );
    } else {
        return (<h2>Enter search request to see weather in your city</h2>);
    }
}

const mapStateToProps = (state, ownProps) => ({
    searchResult: state.searchResult,
    isLoaded: state.searchLoaded
});

export default connect(mapStateToProps, null)(SearchResult);