const initialState = {
    notes: []
};

function reducer(state = initialState, action) {
    switch (action.type) {
        case 'ADD_NOTE':
            let newNotes = [...state.notes];
            newNotes.push(action.payload);
            return {
                ...state,
                notes: newNotes
            }
        case 'REMOVE_NOTE':
            let newNotes2 = [...state.notes];
            newNotes2 = newNotes2.filter(note => note.id !== action.payload)
            return {
                ...state,
                notes: newNotes2
            }
        case 'MARK_DONE':
            let newNotes3 = [...state.notes];
            newNotes3.forEach(note => {
                if (note.id === action.payload) note.isDone = !note.isDone;
            });
            return {
                ...state,
                notes: newNotes3
            }
        default:
            return state;
    }
}

export default reducer;