import React, { Component } from 'react';
import { Provider } from 'react-redux';
import Todolist from './todolist';
import store from '../redux/store';
import { BrowserRouter, Link, Switch, Route } from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <div style={{ margin: '20px' }} className="App">
            <header style={{margin: '10px 0px'}}>
              <Link style={{marginRight: '10px'}} to='/new'>New notes</Link>
              <Link style={{marginRight: '10px'}} to='/done'>Done notes</Link>
              <Link style={{marginRight: '10px'}} to='/all'>All notes</Link>
            </header>
            <Switch>
              <Route path='/' exact />
              <Route path='/new' exact component={Todolist} />
              <Route path='/done' exact component={Todolist} />
              <Route path='/all' exact component={Todolist} />
              <Route render={() => <h1>Notes list not found</h1>} />
            </Switch>
          </div>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
