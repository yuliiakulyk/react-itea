import React from 'react';
import { connect } from 'react-redux';

const Todolist = ({ notes, add, markDone, remove, location }) => {
    let doneStyle = { textDecoration: 'line-through', margin: '5px' };
    let listHeader;
    let notesToShow = [];
    switch (location.pathname) {
        case '/new':
            listHeader = <h1>New notes</h1>;
            notesToShow = notes.filter(note => note.isDone === false);
            break;
        case '/done':
            listHeader = <h1>Done notes</h1>;
            notesToShow = notes.filter(note => note.isDone === true);
            break;
        case '/all':
            listHeader = <h1>All notes</h1>;
            notesToShow = notes;
            break;
        default:
            notesToShow = [];
    }

    return (
        <div style={{ marginTop: '20px' }}>
            <input id='note' type='text' name='note' placeholder='Enter note' autoComplete='off' />
            <button onClick={add}>Add note</button>
            <div>
                {listHeader}
                {(notesToShow.length > 0) ?
                    notesToShow.map(
                        (note, index) => (
                            <p key={index} style={(note.isDone === true) ? doneStyle : { margin: '5px' }}>
                                <button style={{ margin: '2px' }} id={note.id} onClick={markDone}>
                                    {note.isDone ? 'Undo' : 'Done'}
                                </button>
                                <button style={{ margin: '2px' }} id={note.id} onClick={remove}>
                                    Remove
                                </button>
                                {note.text}
                            </p>
                        ))
                    : <p>No notes yet</p>}
            </div>
        </div>)
}

/*
    Redux
*/

const mapStateToProps = (state, ownProps) => ({
    notes: state.notes
});

const mapDispatchToProps = (dispatch, ownProps) => ({
    add: (e) => {
        let text = document.getElementById('note').value;
        document.getElementById('note').value = '';
        dispatch({
            type: 'ADD_NOTE',
            payload: {
                id: new Date().getTime().toString(),
                text: text,
                isDone: false
            }
        });
    },
    markDone: (e) => {
        dispatch({
            type: 'MARK_DONE',
            payload: e.target.id
        });
    },
    remove: (e) => {
        dispatch({
            type: 'REMOVE_NOTE',
            payload: e.target.id
        });
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(Todolist);