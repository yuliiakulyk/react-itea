import React, { Component } from 'react';
import Spinner from 'react-spinner-material';
import './loader-image.css';

class LoaderImage extends Component {

    constructor(props) {
        super(props);

        this.state = {
            loaded: false,
            image: null
        }
        this.handleImageLoad = this.handleImageLoad.bind(this);
    }

    static defaultProps = {
        src: "https://www.resolutiongallery.com/wp-content/themes/trend/assets/img/empty/424x500.png"
    }

    handleImageLoad() {
        this.setState({
            loaded: true
        });
        console.log('image loaded');
    }

    render() {
        let { loaded } = this.state;
        return (
            <div 
                className='center'
                >
                <img
                    className={!loaded ? 'hidden' : 'fitContainer'}
                    src={this.props.src}
                    onLoad={this.handleImageLoad}
                />
                <Spinner 
                size={80} spinnerColor={"#333"} spinnerWidth={2} visible={!loaded} />
            </div>
        );
    }
}

export default LoaderImage;