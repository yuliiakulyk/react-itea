import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import LoaderImage from './loader-image';

class App extends Component {
  render() {
    return (
      <div className="App">
        <LoaderImage 
          src="https://media.springernature.com/lw630/nature-cms/uploads/cms/pages/2913/top_item_image/cuttlefish-e8a66fd9700cda20a859da17e7ec5748.png"
        />
      </div>
    );
  }
}

export default App;
