import React from 'react';

const Row = ({ children, head }) => {

    return (
        <tr>
            {children}
        </tr>
    );
}

export default Row;