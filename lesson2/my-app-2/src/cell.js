import React, { Component } from 'react';

const Cell = ({ type, cells, background, color, currency, head, children }) => {

    let style = { color: color, background: background, padding: '3px' };
    if (type === 'date') style.fontStyle = 'italic';
    if (type === 'number') style.textAlign = 'right';
    if (type === 'money') {
        style.textAlign = 'right';
        if (!currency) {
            console.warn('Currency is missing in props, should be specified if type is money');
        } else {
            children += currency;
        }
    }
    if (type === 'text') style.textAlign = 'left';

    if (head) {
        return <th colspan={cells} style={style}>{children}</th>
    } else {
        return <td colspan={cells} style={style}>{children}</td>
    }
}

Cell.defaultProps = {
    head: false,
    type: 'text',
    cells: 1,
    background: 'transparent',
    color: 'black'
};

export default Cell;