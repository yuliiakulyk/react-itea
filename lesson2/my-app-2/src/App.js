import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Row from './row';
import Cell from './cell';

class App extends Component {
  render() {
    return (
      <div className="App">
        <table style={{ margin: 'auto' }}>
          <Row head={true}>
            <Cell background='black' color='white'>
              I am header cell 1
          </Cell>
            <Cell background='black' color='white'>
              I am header cell 2 with long text
          </Cell>
          </Row>

          <Row>
            <Cell type='date'>
              12/11/2019
          </Cell>
            <Cell type='number'>
              448 455.45
          </Cell>
          </Row>

          <Row>
            <Cell type='money' currency='$'>
              99.99
          </Cell>
            <Cell type='money'>
              10.00
          </Cell>
          </Row>

          <Row>
            <Cell cells={2}>
              This long text will occupy 2 cells
            </Cell>
          </Row>

          <Row>
            <Cell type='money' cells={2}>
              69.99
            </Cell>
          </Row>

        </table>
      </div>
    );
  }
}

export default App;
