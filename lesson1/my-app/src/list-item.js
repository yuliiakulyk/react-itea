import React, { Component } from 'react';

class ListItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            arrived: false
        }

        this.markArrived = this.markArrived.bind(this);
    }

    markArrived() {
        this.setState({ arrived: !this.state.arrived });
    }

    render() {
        const { guest } = this.props;

        return (
            <div className={this.state.arrived ? "list-item done" : "list-item"}>
                <div className="inline">
                    <p>Гость <b>{guest.name}</b> работает в компании <b>"{guest.company}"</b>.</p>
                    <p>Его контакты:</p>
                    <p><b>{guest.phone};</b></p>
                    <p><b>{guest.address}</b></p>
                </div>
                <div className="inline">
                    <button className="btn btn-success" onClick={this.markArrived}>Прибыл</button>
                </div>
            </div>
        );
    }
}

export default ListItem;